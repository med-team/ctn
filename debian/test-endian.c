#include <stdio.h>
#include <stdlib.h>

int
main()
{
  unsigned int i = 1 + (256 * 2) + (256*256 * 3) + (256*256*256 * 4);
  unsigned char* p = (unsigned char*) &i;

  if (*p == 1 && *(p+1) == 2 && *(p+2) == 3 && *(p+3) == 4) {
    printf("LITTLE_ENDIAN_ARCHITECTURE\n");
    exit (0);
  }
  else if (*p == 4 && *(p+1) == 3 && *(p+2) == 2 && *(p+3) == 1) {
    printf ("BIG_ENDIAN_ARCHITECTURE\n");
    exit (0);
  }

  printf ("UNKNOWN_ENDIAN_ARCHITECTURE\n");
  exit(1);
}

  
