ctn (3.2.0~dfsg-8) unstable; urgency=medium

  * Team upload.
  * d/test-endian.c: fix ARCHITECTURE detection with gcc 14.
  * d/rules: ignore newly enforced checks with gcc 14.
    Despite the relative simplicity of fixes for this gcc releast, the ctn
    code base is so large and ancient that the resulting patch for proper
    corrections of build errors is barely manageable.  For this particular
    case, reducing the severity of errors seemed more appropriate, and
    also prudent if considering risks of introducing bugs with the patch.
    (Closes: #1074893)
  * d/control: declare compliance to standards version 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Tue, 23 Jul 2024 22:49:55 +0200

ctn (3.2.0~dfsg-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with -Werror=implicit-function-declaration (Closes: #1066718).

 -- Andrey Rakhmatullin <wrar@debian.org>  Wed, 27 Mar 2024 23:50:34 +0500

ctn (3.2.0~dfsg-7) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Repository.
  * Install config files from debian/tmp location

 -- Andreas Tille <tille@debian.org>  Mon, 16 Nov 2020 13:45:24 +0100

ctn (3.2.0~dfsg-6) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.5
  * Drop unneeded get-orig-source target

 -- Andreas Tille <tille@debian.org>  Thu, 19 Jul 2018 08:02:10 +0200

ctn (3.2.0~dfsg-5) unstable; urgency=medium

  * Update README.Debian
  * debhelper 10
  * d/watch: version=4
  * Build-Depends: libfl-dev
    Closes: #846419
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Thu, 01 Dec 2016 16:05:37 +0100

ctn (3.2.0~dfsg-4) unstable; urgency=medium

  * Move packaging to Git
  * cme fix dpkg-control
  * Revert changes from last NMU
    Closes: #842102
  * Build-Depends: default-libmysqlclient-dev

 -- Andreas Tille <tille@debian.org>  Wed, 26 Oct 2016 09:12:00 +0200

ctn (3.2.0~dfsg-3.1) unstable; urgency=medium

  * Non-maintainer upload
  * Build package with -fPIC CFLAG (Closes: #837417)

 -- Balint Reczey <balint@balintreczey.hu>  Wed, 28 Sep 2016 21:25:59 +0200

ctn (3.2.0~dfsg-3) unstable; urgency=medium

  * cme fix dpkg-control
  * Fix mayhem issues by checking missing parameters
    Closes: #715642, #715749, #715765, #715766, #715782

 -- Andreas Tille <tille@debian.org>  Sun, 20 Dec 2015 21:12:06 +0100

ctn (3.2.0~dfsg-2) unstable; urgency=medium

  * debian/upstream moved to debian/upstream/metadata
  * debian/patches/50_clang_FTBFS_Wreturn-type.patch: Enable building
    with clang (Thanks for the patch to Nicolas Sévelin-Radiguet
     <nicosr@free.fr>)
    Closes: #744085
  * Build-Depends: s/libmysqlclient15-dev/libmysqlclient-dev/
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Sat, 12 Apr 2014 07:31:54 +0200

ctn (3.2.0~dfsg-1) unstable; urgency=low

  [ Charles Plessy ]
  * New upstream version available, on sf.net.
  * debian/watch, debian/copyright, debian/control: updated homepage.

  [ Thorsten Alteholz ]
  * debian/rules: target get-orig-source added

  [ Andreas Tille ]
  * debian/copyright:
     - DEP5
     - Files-Excluded: apps/displays/x_utils.c
       Closes: #724935, #724936
  * debian/source/format: 3.0 (quilt)
  * debian/control:
     - cme fix dpkg-control
     - debhelper 9
     - canonical Vcs URLs
     - Priority: optional
     - s/lesstif2-dev/libmotif-dev/ in Build-Depends
       Closes: #714750, #714758
  * debian/rules: rewritten to use dh and make things more transparent
    by using debian/*.{install,examples} files
  * debian/README.source removed
  * debian/upstream: bibliographic reference
  * debian/ctnmake.debian.mysql.options: drop some unneeded libraries

 -- Andreas Tille <tille@debian.org>  Thu, 07 Nov 2013 14:44:28 +0100

ctn (3.0.6-13) unstable; urgency=low

  [ Andreas Tille ]
  * Group maintenance by Debian-Med Packaging Team
  * Added myself to uploaders
  * DM-Upload-Allowed: yes
  * Fixed Vcs fields
  * Moved Homepage from long description to separate field
    (Closes: #466671).
  * Patch to make the ctn working properly with mysql server > 4.2.
    Thanks to Pablo Sau <psau@cadpet.es> (Closes: #326916).

  [ Steve M. Robbins ]
  * Remove self from uploaders.

  [ Thijs Kinkhorst ]
  * Drop empty prerm/postinst scripts.
  * Replace obsolete x-dev with x11-proto-core-dev
  * Use quilt to manage patches, add proper dependencies and
    targets in debian/rules. All patches are still in one file,
    which needs to be split out over time.
  * Checked for policy version 3.8.0, add README.source.
  * Remove self from uploaders, but upload for one last time ;-)

 -- Thijs Kinkhorst <thijs@debian.org>  Fri, 25 Jul 2008 18:03:57 +0200

ctn (3.0.6-12) unstable; urgency=low

  * Do not bail out if libsrc symlinks already exist, since this
    can cause problems in some build order.
  * Minor packaging cleanups.

 -- Thijs Kinkhorst <thijs@debian.org>  Thu, 26 Jul 2007 14:15:04 +0200

ctn (3.0.6-11) unstable; urgency=low

  [ Charles Plessy ]
  * Fixed subversion repository declaration in debian/control.

  [ Thijs Kinkhorst ]
  * Replace deprecated ${Source-Version} with ${binary:Version}.

 -- Thijs Kinkhorst <thijs@debian.org>  Mon, 18 Jun 2007 10:36:21 +0200

ctn (3.0.6-10) unstable; urgency=medium

  * Fixing build on 64bit platforms and providing *INT preprocessor
    definitions for the applications built using ctn-dev.
    Patch by Yaroslav Halchenko, thanks! (Closes: 387183)

 -- Thijs Kinkhorst <thijs@debian.org>  Sat, 27 Jan 2007 19:23:09 +0100

ctn (3.0.6-9) unstable; urgency=low

  * Acknowledge NMU (Closes: #388612), thanks Steinar Gunderson!
  * Add Subversion repository URL to debian/control.
  * Add note to README.Debian about LONGSIZE on 64 bit platforms,
    courtesy Michael Hanke.

 -- Thijs Kinkhorst <thijs@debian.org>  Sun, 24 Sep 2006 13:25:00 +0200

ctn (3.0.6-8.1) unstable; urgency=medium

  * Non-maintainer upload.
  * In the LONG_WORD structure, always use unsigned int since we want a 32-bit
    variable, and int is 32 bits on all platforms supported by Debian (it used
    to be unsigned long for all platforms except alpha, which broke on
    platforms such as ppc64 and amd64). (Closes: #387183)
  * Build-depend on lesstif2-dev instead of lesstif-dev, since the latter is
    obsolete.

 -- Steinar H. Gunderson <sesse@debian.org>  Thu, 21 Sep 2006 15:40:02 +0200

ctn (3.0.6-8) unstable; urgency=low

  * Add Steve M. Robbins as a co-maintainer.
  * Clarify description.
  * Remove unneeded cruft from debian/rules.
  * Update maintainer address.
  * Checked for standards-version 3.7.2, no changes necessary.

 -- Thijs Kinkhorst <thijs@debian.org>  Wed,  5 Jul 2006 15:11:40 +0200

ctn (3.0.6-7) unstable; urgency=low

  * Update to libmysqlclient15-dev (Closes: #343763).

  * Add README.mysql-versions with some explanation about ctn not working
    with MySQL 4.1+ yet.

 -- Thijs Kinkhorst <kink@squirrelmail.org>  Thu, 29 Dec 2005 16:05:16 +0100

ctn (3.0.6-6) unstable; urgency=low

  * Change build-dependency on xlibs-dev to libx11-dev,
    libxt-dev, x-dev since xlibs-dev is going away.

  * Upgrade debhelper compatibility to the recommended level 5.

  * Clean debian/rules of commented-out commands.

 -- Thijs Kinkhorst <kink@squirrelmail.org>  Tue, 13 Dec 2005 23:32:21 +0100

ctn (3.0.6-5) unstable; urgency=low

  * Fix test-endian.c for 64-bit big endian architectures, thanks
    Andreas Jochens (Closes: #319514).

 -- Thijs Kinkhorst <kink@squirrelmail.org>  Tue, 26 Jul 2005 00:01:41 +0200

ctn (3.0.6-4) unstable; urgency=high

  * Fix incorrect dependencies by using dh_shlibdeps.
    (RC, high urgency. Closes: #317985)
  * Add watch file.
  * Update Standards-Version to 3.6.2, no changes required.

 -- Thijs Kinkhorst <kink@squirrelmail.org>  Wed, 13 Jul 2005 19:11:33 +0200

ctn (3.0.6-3) unstable; urgency=low

  * New maintainer, deorphans package. (Closes: #297419)

 -- Thijs Kinkhorst <kink@squirrelmail.org>  Mon,  7 Mar 2005 21:52:54 +0100

ctn (3.0.6-2) unstable; urgency=low

  * Orphan as in bug #297419
  * Update to libmysqlclient14

 -- Kevin M. Rosenberg <kmr@debian.org>  Sat,  5 Mar 2005 02:53:17 -0700

ctn (3.0.6-1) unstable; urgency=low

  * New upstream

 -- Kevin M. Rosenberg <kmr@debian.org>  Fri, 29 Aug 2003 01:05:50 -0600

ctn (3.0.5-2) unstable; urgency=low

  * Change priority to extra

 -- Kevin M. Rosenberg <kmr@debian.org>  Fri, 31 Jan 2003 13:30:53 -0700

ctn (3.0.5-1) unstable; urgency=low

  * New upstream
  * Compile with gcc-3.2
  * Update standards-version to 3.5.8.0: move changes compared to upstream
  from copyright to README.Debian

 -- Kevin M. Rosenberg <kmr@debian.org>  Fri, 24 Jan 2003 04:03:12 -0700

ctn (3.0.4-8) unstable; urgency=low

  * Add default manpage.

 -- Kevin M. Rosenberg <kmr@debian.org>  Wed, 25 Sep 2002 11:01:54 -0600

ctn (3.0.4-7) unstable; urgency=low

  * Update e-mail address
  * Update Standards Version
  * Add man page for dcm_resize

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun, 11 Aug 2002 23:53:31 -0600

ctn (3.0.4-6) unstable; urgency=low

  * Fix bug in upstream Makefile

  * Reformat debian/copyright

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun,  7 Jul 2002 06:39:09 -0600

ctn (3.0.4-5) unstable; urgency=low

  * Added info to copyright file, README.Debian, and description

  * Added undocumented manpages for remaining CTN binaries so
  they can be included in the package. Removed notes about
  only a minority of the binary programs being installed.

  * Put shell scripts in /usr/share/ctn/examples/ directory

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 27 Jun 2002 00:36:27 -0600

ctn (3.0.4-4) unstable; urgency=low

  * Added zlib1g-dev as a build dependency (closes: 151067)

  * debhelper V4

  * Added bison to build dependencies

 -- Kevin M. Rosenberg <kmr@debian.org>  Wed, 26 Jun 2002 15:04:04 -0600

ctn (3.0.4-3) unstable; urgency=low

  * Update copyright file (closes: 150728)

 -- Kevin M. Rosenberg <kmr@debian.org>  Mon, 24 Jun 2002 22:23:50 -0600

ctn (3.0.4-2) unstable; urgency=low

  * Moved the "csh | c-shell" dependency to correct line (closes: 150902)

 -- Kevin M. Rosenberg <kmr@debian.org>  Mon, 24 Jun 2002 18:13:26 -0600

ctn (3.0.4-1) unstable; urgency=low

  * New upstream version

  * Added ctn-doc as a Suggest: control field for ctn and ctn-dev

  * Rewrite csh scripts to use bash

  * Changed build-dependencies in control file (closes: 149194)

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 20 Jun 2002 04:40:07 -0600

ctn (3.0.3-1) unstable; urgency=low

  * Patched upstream files for Debian: removed runtime dependencies on
    environment variable CTN_TARGET. Use default default, standard Debian
    directory if CTN_TARGET is not specified at run-time.

  * Started writing manual pages for the CTN binaries.

  * Initial upload. (closes: #145878)

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun,  5 May 2002 06:07:24 -0600
