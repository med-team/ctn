#!/bin/bash
#-------------------------- =+- Shell script -+= --------------------------
#
# @file      intsizes_h.sh
# @date      Fri Jan 26 15:18:19 2007
# @brief
#
#
#  Yaroslav Halchenko                                      CS@UNM, CS@NJIT
#  web:     http://www.onerussian.com                      & PSYCH@RUTGERS
#  e-mail:  yoh@onerussian.com                              ICQ#: 60653192
#
# DESCRIPTION (NOTES):
#
# COPYRIGHT: Yaroslav Halchenko 2007
#
# LICENSE:
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the 
#  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# On Debian system see /usr/share/common-licenses/GPL for the full license.
#
#-----------------\____________________________________/------------------
export `debian/test-sizes`

cat > debian/intsizes.h <<EOF
#ifndef INTSIZES_IS_IN
#define INTSIZES_IS_IN 1

#ifdef  __cplusplus
extern "C" {
#endif

# define INTSIZE $INTSIZE
# define LONGSIZE $LONGSIZE
# define SHORTSIZE $SHORTSIZE

#ifdef  __cplusplus
}
#endif

#endif
EOF
